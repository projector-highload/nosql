package main

import (
	"context"
	"log"
	"time"

	"github.com/olivere/elastic/v7"

	"github.com/brianvoe/gofakeit/v6"
)

const indexName = "twitter"

const mapping = `
{
  "settings": {
    "number_of_shards": 10,
    "analysis": {
      "analyzer": {
        "autocomplete": {
          "tokenizer": "autocomplete",
          "filter": [
            "lowercase"
          ]
        },
        "autocomplete_search": {
          "tokenizer": "lowercase"
        }
      },
      "tokenizer": {
        "autocomplete": {
          "type": "edge_ngram",
          "min_gram": 2,
          "max_gram": 10,
          "token_chars": [
            "letter"
          ]
        }
      }
    }
  },
  "mappings": {
    "properties": {
      "user": {
        "type": "keyword"
      },
      "message": {
        "type": "text",
        "store": true,
        "analyzer": "autocomplete",
        "search_analyzer": "autocomplete_search",
		"fields": {
			"complete": {
				"type": "completion"
			}
		}
      },
      "image": {
        "type": "keyword"
      },
      "created": {
        "type": "date"
      },
      "tags": {
        "type": "keyword"
      },
      "location": {
        "type": "geo_point"
      },
      "suggest_field": {
        "type": "search_as_you_type"
      }
    }
  }
}`

type GeoPoint struct {
	Latitude  float64 `json:"lat"`
	Longitude float64 `json:"lon"`
}

type Tweet struct {
	User         string    `json:"user"`
	Message      string    `json:"message"`
	Image        string    `json:"image"`
	CreatedAt    time.Time `json:"created_at"`
	Tags         []string  `json:"tags"`
	Location     GeoPoint  `json:"location"`
	SuggestField string    `json:"suggest_field"`
}

func main() {
	// Starting with elastic.v5, you must pass a context to execute each service
	ctx := context.Background()

	// Obtain a client and connect to the default Elasticsearch installation
	// on 127.0.0.1:9200. Of course you can configure your client to connect
	// to other hosts and configure it in various other ways.
	client, err := elastic.NewClient()
	if err != nil {
		// Handle error
		panic(err)
	}

	// Ping the Elasticsearch server to get e.g. the version number
	info, code, err := client.Ping("http://127.0.0.1:9200").Do(ctx)
	if err != nil {
		// Handle error
		panic(err)
	}

	exists, err := client.IndexExists(indexName).Do(ctx)
	if err != nil {
		// Handle error
		panic(err)
	}

	if !exists {
		if err := createIndex(ctx, client); err != nil {
			panic(err)
		}
	}

	faker := gofakeit.New(time.Now().UnixNano())

	for i := 0; i < 10_000; i++ {
		tweet := makeTweet(faker)

		_, err := client.Index().
			Index(indexName).
			Id(faker.UUID()).
			BodyJson(tweet).
			Do(ctx)
		if err != nil {
			// Handle error
			panic(err)
		}
	}

	log.Println(info, code)
}

func makeTweet(faker *gofakeit.Faker) Tweet {
	geo := GeoPoint{
		Latitude:  gofakeit.Latitude(),
		Longitude: gofakeit.Longitude(),
	}

	msg := faker.HipsterSentence(5)

	return Tweet{
		User:         faker.Username(),
		Message:      msg,
		Image:        faker.ImageURL(128, 128),
		CreatedAt:    faker.Date(),
		Tags:         []string{faker.Word()},
		Location:     geo,
		SuggestField: msg,
	}
}

func createIndex(ctx context.Context, client *elastic.Client) error {
	_, err := client.
		CreateIndex(indexName).
		BodyString(mapping).
		Do(ctx)

	return err
}
