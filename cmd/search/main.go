package main

import (
	"bufio"
	"context"
	"fmt"
	"os"
	"os/signal"
	"reflect"
	"strings"
	"syscall"

	"github.com/olivere/elastic/v7"
)

type Result struct {
	Message string `json:"message"`
}

func main() {
	ctx := context.Background()

	client, err := elastic.NewClient()
	if err != nil {
		// Handle error
		panic(err)
	}

	sigs := make(chan os.Signal, 1)

	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM)

	go func() {
		reader := bufio.NewReader(os.Stdin)

		for {
			fmt.Print("Enter search phrase: ")
			searchString, err := reader.ReadString('\n')
			if err != nil {
				fmt.Fprintln(os.Stderr, err)
			}

			searchString = strings.Trim(searchString, "\n")

			termQuery := elastic.NewMatchQuery("message", searchString)

			suggest := elastic.
				NewCompletionSuggester("autocomplete").
				Field("message.complete").
				Prefix(searchString).
				Fuzziness("AUTO")

			searchResult, err := client.Search().
				Index("twitter"). // search in index "twitter"
				Query(termQuery). // specify the query
				Suggester(suggest).
				From(0).
				Size(10).     // take documents 0-9
				Pretty(true). // pretty print request and response JSON
				Do(ctx)       // execute
			if err != nil {
				// Handle error
				panic(err)
			}

			if len(searchResult.Hits.Hits) > 0 {
				fmt.Println("Search results: ")
				var msg Result
				for _, r := range searchResult.Each(reflect.TypeOf(msg)) {
					fmt.Println(r.(Result).Message)
				}
			}

			for name, ss := range searchResult.Suggest {
				if len(ss) == 0 {
					continue
				}

				fmt.Println("Suggestions for " + name + ":")

				for _, s1 := range ss {
					for _, o := range s1.Options {
						fmt.Println(o.Text)
					}
				}

			}

			fmt.Print("\n")
		}
	}()

	<-sigs

	fmt.Print("\n")
}
